DROP TABLE IF EXISTS "utilisateur";

CREATE TABLE utilisateur(
    id SERIAL PRIMARY KEY, 
    nom VARCHAR(250) NOT NULL,
    prenom VARCHAR(250) NOT NULL,
    password VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL
);

