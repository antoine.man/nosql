var db = connect("mongodb://user:pass@127.0.0.1:27017");

db = db.getSiblingDB('noSQL'); // we can not use "use" statement here to switch db

db.createCollection('Trains');

db.Trains.insert({
    id:1,
        DateD: "2021-12-12",
        GareD: {
            Ville: "Etampes",
            CoordGPS: "48.434416:2.16142",
            IdSNCF: "1"
        },
        GareA: {
            Ville: "Gare du Nord",
            latitude: 48.8809,
            longitude: 2.3553,
            IdSNCF: "2"
        },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
        Infos: {
            Capacite: 869,
            Revision: "2021-08-12",
            Defauts: "Aucun",
            Secteur: "Ile-de-France"
        }
    });
db.Trains.insert({
        id:2,
        DateD: "2022-01-01",
        GareD: {
            Ville: "Gare du Nord",
            latitude: 48.8809,
            longitude: 2.3553,
            IdSNCF: "2"
        },
        GareA: {
            Ville: "Etampes",
            CoordGPS: "48.434416:2.16142",
            IdSNCF: "1"
        },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
        Infos: {
            Capacite: 650,
            Revision: "2021-08-12",
            Defauts: "Aucun",
            Secteur: "Ile-de-France"
        }
    });
db.Trains.insert({
    id:3,
        DateD: "2021-12-29",
        GareD: {
            Ville: "Etampes",
            latitude: 48.8809,
            longitude: 2.3553,
            IdSNCF: "2"
        },
        GareA: {
            Ville: "Gare de Lyon",
            CoordGPS: "48.434416:2.16142",
            IdSNCF: "1"
        },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
        Infos: {
            Capacite: 800,
            Revision: "2021-08-12",
            Defauts: "Aucun",
            Secteur: "Ile-de-France"
        }
    }
);
db.Trains.insert({
    id:4,
    DateD: "2021-12-12",
    GareD: {
        Ville: "Etampes",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Gare du Nord",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 324,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});
db.Trains.insert({
    id:5,
    DateD: "2021-13-12",
    GareD: {
        Ville: "Etampes",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Gare du Nord",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 900,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});
db.Trains.insert({
    id:6,
    DateD: "2021-14-12",
    GareD: {
        Ville: "Etampes",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Gare du Nord",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 146,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});
db.Trains.insert({
    id:7,
    DateD: "2021-15-12",
    GareD: {
        Ville: "Etampes",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Gare du Nord",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 125,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});

db.Trains.insert({
    id:8,
    DateD: "2021-20-12",
    GareD: {
        Ville: "Nantes",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Paris",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 50,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});

db.Trains.insert({
    id:9,
    DateD: "2021-22-12",
    GareD: {
        Ville: "Bebel",
        CoordGPS: "48.434416:2.16142",
        IdSNCF: "1"
    },
    GareA: {
        Ville: "Mana",
        latitude: 48.8809,
        longitude: 2.3553,
        IdSNCF: "2"
    },
    Reservations: [
        {Nom: "Chaillou", Prenom:"Louis", Email:"Louischaillou1@gmail.com", NombreDePlaces: 8},
        {Nom: "Man", Prenom:"Antoine", Email:"AntoineMan@gmail.com",NombreDePlaces: 1},
        {Nom: "Admin", Prenom:"Admin", Email:"Admin@gmail.com",NombreDePlaces: 5}
    ],
    Infos: {
        Capacite: 20,
        Revision: "2021-08-12",
        Defauts: "Aucun",
        Secteur: "Ile-de-France"
    }
});