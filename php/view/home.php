<?php
    if (isset($_SESSION['user'])) 
    {
        $user = $_SESSION['user'];
    }
?>

<!DOCTYPE html>
<html lang="en">

<?php include('./view/front/head.html'); ?>

<body id="page-top">

    <?php include('./view/front/header.html'); ?>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('./view/front/sidebar.html'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include('./view/front/topbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    
	                    <!-- Approach -->
	                    <div class="card shadow mb-4">
	                        <div class="card-header py-3">
	                            <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-suitcase"></i> Planifiez votre itinéraire </h6>
	                        </div>
	                        <div class="card-body">

                                <?php if (isset($data['message_error'])) { ?>
                                    
                                    <div class="form-group">
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong><?php echo $data['message_error'] ?></strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>

                                <?php 
                                } 
                                ?>
                                
                                <?php if (isset($data['message_success'])) { ?>
                                    
                                    <div class="form-group">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong><?php echo $data['message_success'] ?></strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>

                                <?php
                                } 
                                ?>
	                        	
	                            <form action="?controller=Trains&action=search" method="post">

	                            	<div class="form-group row">
	                                    <div class="col-sm-6 mb-3 mb-sm-0">
	                                    	<label>Départ</label>
	                                        <input type="text" class="form-control form-control-user" id="exampleFirstName" name="depart">
	                                    </div>
	                                    <div class="col-sm-6">
	                                    	<label>Destination</label>
	                                        <input type="text" class="form-control form-control-user" id="exampleLastName" name="destination">
	                                    </div>
	                                </div>
	                                
                                    <div class="form-group row">
                                        <div class="col-sm-6 ">
                                            <label>Date de départ</label>
                                            <input type="date" class="form-control form-control-user" id="exampleFirstName" name="dateDepart" required>
                                        </div>
                                    </div>
	                                
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label>Quantité</label>
                                            <input type="number" class="form-control form-control-user" id="exampleLastName" min="0" name="quantite" required>
                                        </div>
                                    </div>
                                    
                                    <input class="btn btn-primary btn-user btn-block" type="submit" value="Rechercher">
	                            </form>
	                        </div>
	                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Prêt à nous quitter ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span>
                    </button>
                </div>
                <div class="modal-body">Cliquer sur "Se déconnecter" afin de mettre fin à votre session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                    <a class="btn btn-primary" href="?controller=ControllerUser&action=logout">Se déconnecter</a>
                </div>
            </div>
        </div>
    </div>

    <?php include('./view/front/footer.html'); ?>

</body>

</html>